package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiplicationTables {
    @GetMapping("/api/tables/multiply")
    public String multiplicationtables(){
        StringBuilder str=new StringBuilder();
        for(int i=1;i<=9;i++){
            for(int j=1;j<=i;j++){
                str=str.append(i).append("*").append(j).append("=").append(i*j);
                if(i+j>=10){
                    str.append("&nbsp");
                }else{
                    str.append("&nbsp&nbsp");
                }
            }
            str.append("<br>");
        }
        return str.toString();
    }

}
